#!/usr/bin/env python

import sys

fn = sys.argv[1] #file to be tranlated
on = sys.argv[2] #file to be saved
maxrole = sys.argv[3] # max number of roles
maxtime = sys.argv[4] # max number of timeslots for timer
effectivemaxtime = sys.argv[5] 
qr = sys.argv[6]    

i_file = open(fn, 'r')
o_file = open(on, 'w')


r2a = { 
1 : 'a', 2 : 'b', 3 : 'c', 4 : 'd' , 5 : 'e' , 6 : 'f', 7 : 'g', 
8 : 'h', 9 : 'i' , 10 : 'Z' , 11 : 'k' , 12 : 'l', 13 : 'm', 14 : 'n', 
15 : 'o', 16 : 'p', 17 : 'q', 18 : 'r', 19 : 's', 20 : 't', 21 : 'u', 
22 : 'v', 23 : 'w', 24 : '#', 25 : '$', 26 : 'z', 
27 : 'A', 28 : 'B', 29 : 'C', 30 : 'D' , 31 : 'E' , 32 : 'F', 33 : 'G', 
34 : 'H', 35 : 'I' , 36 : 'J' , 37 : 'K' , 38 : 'L', 39 : 'M', 40 : 'N', 
41 : 'O', 42 : 'P', 43 : 'Q', 44 : 'R', 45 : 'S', 46 : 'T', 47 : 'U', 
48 : 'V', 49 : 'W', 50 : 'X', 51 : 'Y', 52 : '@' 
}


o_file.write(":smt (define-type uat (tuple ")
j=1
while j<=150 : 
    o_file.write("bool ")
    j=j+1
o_file.write("))\n\n")


o_file.write(":smt (define all_false::uat (mk-tuple ")
j=1
while j<=150 : 
    o_file.write("false ")
    j=j+1
o_file.write("))\n\n")
print

numOfVars = 0 
numOfVars = int(round((int(maxrole) * int(effectivemaxtime))/150)) + 1

j = 1
while j<=numOfVars :
    o_file.write(":local " + r2a[j] + " uat \n")
    j=j+1
o_file.write("\n")


j = 1 
while j<=numOfVars :
    
    o_file.write(":local " + r2a[j + 26] + " uat \n")
    j=j+1
o_file.write("\n")

  
o_file.write(":global z real \n")  
o_file.write("\n")


o_file.write(":initial\n")
o_file.write(":var x\n")
o_file.write(":cnj (= z[x] 1) ")

j = 1
while j<=numOfVars :
    o_file.write("(= " + r2a[j] + "[x] all_false) ")
    j=j+1

j = 1
while j<=numOfVars :
    o_file.write("(= " + r2a[j + 26] + "[x] all_false) ")
    j=j+1

o_file.write("\n\n")

o_file.write(":unsafe\n")
o_file.write(":var x\n")
o_file.write(":cnj")

if (qr == "ur"):
    varIndex = int(round((((int(sys.argv[7]) - 1) * int(effectivemaxtime)) + int(sys.argv[8]))/150)) + 1
    roleIndex = (((int(sys.argv[7]) - 1) * int(effectivemaxtime)) + int(sys.argv[8]))%150
    
    if (roleIndex == 0):
        varIndex = varIndex - 1
        roleIndex = 150

    o_file.write(" (= (select " + r2a[varIndex] + "[x] %i) true)" % roleIndex)

o_file.write("\n")


i_file.seek(0, 0)
tr=1
for line in i_file:
    isAsgnorRevk = False   
    if (line =="\n"):
	break
    tok_lst = line.split()
    o_file.write("\n")
    o_file.write(":comment %i\n" % tr)
    tr=tr+1
    o_file.write(":transition\n")
    o_file.write(":var x\n")
    if ((tok_lst[0] == "can_assign") or (tok_lst[0] == "can_revoke")):
	isAsgnorRevk = True
	o_file.write(":var y\n")
    else:
	o_file.write(":var y\n")
    o_file.write(":var j\n")

    ###Print guard
    if (tok_lst[1][0] == "t"):
	o_file.write(":guard (= z[y] " + tok_lst[3][1:] + ") ")
    elif (tok_lst[1][0] == "f"):
	o_file.write(":guard (not (= y y)) (= z[y] " + tok_lst[3][1:] + ") ")
    else :
    	varIndexG = int(round((((int(tok_lst[1]) - 1) * int(effectivemaxtime)) + int(tok_lst[3][1:]))/150)) + 1
	    
    	roleIndexG = (((int(tok_lst[1]) - 1) * int(effectivemaxtime)) + int(tok_lst[3][1:]))%150
    	if (roleIndexG == 0):
       	    varIndexG = varIndexG - 1
       	    roleIndexG = 150
    
   	o_file.write(":guard (= z[y] " + tok_lst[3][1:] + ") (= (select " + r2a[varIndexG] + "[y] " + str(roleIndexG) + ") true) (= (select " + r2a[varIndexG + 26] + "[y] " + str(roleIndexG) + ") true) ")
    


    #####

    update=0
    
    
    asgtimeindex = tok_lst.index(";")
    asgtime = tok_lst[asgtimeindex + 1]
    can_assign = -1
    can_enable = -1
    for i,tok in enumerate(tok_lst): 
		
        if (i==0 and tok=="can_assign") : 
	    can_assign=1
        elif (i==0 and tok=="can_revoke") : 	    
	    can_assign=0
	elif (i==0 and tok=="can_enable") : 	    
	    can_enable=1
	elif (i==0 and tok=="can_disable") : 	    
	    can_enable=0
	elif (i>0 and tok==";") : 
            update = update + 1
        elif (i>0 and tok==",") : 
            update = update + 1
	    if (update == 4):
	    	o_file.write("\n")
	    	o_file.write(":numcases 3\n")
            	o_file.write(":case (= x j)\n")
        
        elif (i>0 and update==2 and tok[0]=='f') : 
            o_file.write(" (not (= x x)) ")
	elif (i>0 and update==2 and tok[0]=='t') : 
            o_file.write(" (= x x) ")
        elif (i>0 and update==2 and tok[0]=='-') :
	    varIndex = int(round((((int(tok[1:]) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
	    
    	    roleIndex = (((int(tok[1:]) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
    	    
    	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150

	    if (isAsgnorRevk == False): 
		varIndex = varIndex + 26 

 	    o_file.write(" (= (select " + r2a[varIndex] + "[x] %i) false)" % roleIndex)
            
        elif (i>0 and update==2 and tok[0]!='-') : 
	    varIndex = int(round((((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
    	    roleIndex = (((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
    	    
    	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150

	    if (isAsgnorRevk == False): 
		varIndex = varIndex + 26 

 	    o_file.write(" (= (select " + r2a[varIndex] + "[x] %i) true)" % roleIndex)

            
        elif (i>0 and update==4 and can_assign==1) : 
	    
	    varIndex = int(round((((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
	    roleIndex = (((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150
		
	    j = 1
    	    while j<=numOfVars :
		if (j == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j] + "[j] " + str(roleIndex) + " true)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1

	    o_file.write(" :val z[j] \n")
	   
	    o_file.write(":case (= y j)\n")
	    j = 1
    	    while j<=numOfVars :
        	o_file.write(" :val " + r2a[j] + "[j] \n")
		j=j+1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1

	    o_file.write(" :val z[j] \n")


    	    o_file.write(":case \n")
    	    j = 1
    	    while j<=numOfVars :
        	o_file.write(" :val " + r2a[j] + "[j] \n")
		j=j+1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1

	    o_file.write(" :val z[j] \n")

	elif (i>0 and update==4 and can_assign==0) : 
            varIndex = int(round((((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
	    roleIndex = (((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150
		
	    j = 1
    	    while j<=numOfVars :
		if (j == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j] + "[j] " + str(roleIndex) + " false)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1

	    o_file.write(" :val z[j] \n")


	    o_file.write(":case (= y j)\n")
    	    j = 1
    	    while j<=numOfVars :
        	o_file.write(" :val " + r2a[j] + "[j] \n")
		j=j+1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    o_file.write(" :val z[j] \n")



    	    o_file.write(":case \n")
    	    j = 1
    	    while j<=numOfVars :
        	o_file.write(" :val " + r2a[j] + "[j] \n")
		j=j+1
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    o_file.write(" :val z[j] \n")
	elif (i>0 and update==4 and can_enable==1) : 
	    
	    varIndex = int(round((((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
	    roleIndex = (((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150
	    varIndex = varIndex + 26  

	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	
	    j = 1
    	    while j<=numOfVars :
		if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " true)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")


	    o_file.write(":case (= y j)\n")
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
    	    j = 1
    	    while j<=numOfVars :
        	if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " true)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")




    	    o_file.write(":case \n")
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
    	    j = 1
    	    while j<=numOfVars :
        	if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " true)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")

	elif (i>0 and update==4 and can_enable==0) : 
            varIndex = int(round((((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))/150)) + 1
	    roleIndex = (((int(tok) - 1) * int(effectivemaxtime)) + int(asgtime[1:]))%150
	    if (roleIndex == 0):
        	varIndex = varIndex - 1
        	roleIndex = 150

	    varIndex = varIndex + 26  
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	
	    j = 1
    	    while j<=numOfVars :
		if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " false)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")


	    o_file.write(":case (= y j)\n")
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
    	    j = 1
    	    while j<=numOfVars :
		if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " false)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")



    	    o_file.write(":case \n")
	    
	    j = 1
    	    while j<=numOfVars :
		o_file.write(" :val " + r2a[j] + "[j] \n")
		j = j + 1
	    
    	    j = 1
    	    while j<=numOfVars :
		if ((j + 26) == varIndex) :
  	    	    o_file.write(" :val " + "(update " + r2a[j + 26] + "[j] " + str(roleIndex) + " false)" + " \n")
		else:
            	    o_file.write(" :val " + r2a[j + 26] + "[j] \n")
		j = j + 1
	    
	    o_file.write(" :val z[j] \n")


#Time passing
print "Note: Using agtrbac2mcmt1.py - with e.g., (= z[x] 1) then 2 ..."
print

tj=1
while tj <= int(maxtime) :

    o_file.write("\n:comment %i\n" % tr)
    tr=tr+1
    o_file.write(":transition\n")
    o_file.write(":var x\n")
    o_file.write(":var j\n")
    o_file.write(":guard (= z[x] " + str(tj)+ ")")
    o_file.write("\n")
    o_file.write(":numcases 2\n")
    o_file.write(":case (= x j)\n")
    j = 1
    while j<=numOfVars :
    	o_file.write(" :val " + r2a[j] + "[j] \n")
    	j=j+1
    j = 1
    while j<=numOfVars :
    	o_file.write(" :val " + r2a[j + 26] + "[j] \n")
    	j=j+1

    if (tj < int(maxtime)):
    	o_file.write(" :val " + str(tj + 1)+ "\n")
    else:
    	o_file.write(" :val 1\n")
    o_file.write(":case\n")
    j = 1
    while j<=numOfVars :
    	o_file.write(" :val " + r2a[j] + "[j] \n")
    	j=j+1
    j = 1
    while j<=numOfVars :
    	o_file.write(" :val " + r2a[j + 26] + "[j] \n")
    	j=j+1

    if (tj < int(maxtime)):
    	o_file.write(" :val " + str(tj + 1)+ "\n")
    else:
    	o_file.write(" :val 1\n")
    tj = tj + 1


i_file.close()
o_file.close()


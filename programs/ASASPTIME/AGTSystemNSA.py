#!/usr/bin/env python

import sys
import os

fn = sys.argv[1] #file to be run
qr = sys.argv[2] #query
#sys.argv[3] : maxrole
#sys.argv[4] : maxtime
#sys.argv[5] : goal
#sys.argv[6] : time


os.system('echo "Translating now..."' )
os.system('echo "==========================================="' )

if (qr == "ur"): 
    
    i_file = open(fn, 'r')
    o_file = open('system', 'w')

    i_file.seek(0, 0)
    tr=0
    
    AssgnGoalTime = []  
    EnableRoleTime = []     
    
    CurrentActiveRolesTimes = []    
    CurrentActiveEnableRT = []    
    NextActiveRolesTimes = []
    NextActiveEnableRT = []
    ActiveRolesTimes = []
    ActiveEnableRT = []
    for line in i_file:
        tok_lst = line.split(' ')
        asgtimeindex = tok_lst.index(";")
        
        if (tok_lst[0] == "can_assign"):  
            AssgnGoalTime.append(str.strip(tok_lst[asgtimeindex + 3]) + '+' + str.strip(tok_lst[asgtimeindex + 1]))
        if (tok_lst[0] == "can_enable"):  
            EnableRoleTime.append(str.strip(tok_lst[asgtimeindex + 3]) + '+' + str.strip(tok_lst[asgtimeindex + 1]))
    

    print (AssgnGoalTime)
    
    CurrentActiveRolesTimes.append(sys.argv[5] + "+t" + sys.argv[6])
    CurrentAction = []
    preCurrentAction = []
    while (True):
        i_file.seek(0, 0)
        preCurrentAction = CurrentAction
        CurrentAction = []    
    
        for line in i_file:
            ActiveRolesTimes = []
            ActiveEnableRT = []
            if (line =="\n"):
                break
            tok_lst = line.split()
            asgtimeindex = tok_lst.index(";")
            asgtime = tok_lst[asgtimeindex + 1]
            targetRole = tok_lst[asgtimeindex + 3]
            strRoleTime = str.strip(tok_lst[asgtimeindex + 3]) + '+' + str.strip(tok_lst[asgtimeindex + 1])

        
    
            if ((tok_lst[0] == "can_assign") and (strRoleTime in CurrentActiveRolesTimes)):
                isAdded = True
                if (tok_lst[1] != 'true'):        
                    strAdminTime = str.strip(tok_lst[1]) + '+' + str.strip(tok_lst[3])
                ActiveRolesTimes.append(strAdminTime)
                ActiveEnableRT.append(strAdminTime)
                if (not(strAdminTime in AssgnGoalTime) or not(strAdminTime in EnableRoleTime)):
                    isAdded = False
    
                j = 5
                canStartCandidate = True        
                while (j < asgtimeindex and isAdded == True):
                    if (tok_lst[j] == targetRole): 
                        isAdded = False  
                        break    
                    if (tok_lst[j][0] != '-' and tok_lst[j] != 'true'):
                        ActiveRolesTimes.append(str.strip(tok_lst[j]) + "+" + asgtime)
                        if (not((str.strip(tok_lst[j]) + "+" + asgtime) in AssgnGoalTime)) :
                            isAdded = False
                            break
                
                    j = j + 1
            
                if (isAdded) :
                    CurrentAction.append(" ".join(tok_lst) + "\n")
                NextActiveRolesTimes.extend(ActiveRolesTimes)
                NextActiveEnableRT.extend(ActiveEnableRT)
                    
            elif ((tok_lst[0] == "can_enable") and (strRoleTime in CurrentActiveEnableRT)):
                isAdded = True
                if (tok_lst[1] != 'true'):        
                    strAdminTime = str.strip(tok_lst[1]) + '+' + str.strip(tok_lst[3])
                ActiveRolesTimes.append(strAdminTime)
                ActiveEnableRT.append(strAdminTime)
                if (not(strAdminTime in AssgnGoalTime) or not(strAdminTime in EnableRoleTime)):
                    isAdded = False
    
                j = 5
                canStartCandidate = True        
                while (j < asgtimeindex and isAdded == True):
                    if (tok_lst[j] == targetRole): 
                        isAdded = False  
                        break    
                if (tok_lst[j][0] != '-' and tok_lst[j] != 'true'):
                    ActiveEnableRT.append(str.strip(tok_lst[j]) + "+" + asgtime)
                    if (not((str.strip(tok_lst[j]) + "+" + asgtime) in EnableRoleTime)) :
                        isAdded = False
                        break
            
                j = j + 1
            
                if (isAdded) :
                    CurrentAction.append(" ".join(tok_lst) + "\n")
                NextActiveRolesTimes.extend(ActiveRolesTimes)
                NextActiveEnableRT.extend(ActiveEnableRT)


    if (len(CurrentAction) <= len(preCurrentAction)):
        break
    CurrentActiveRolesTimes.extend(NextActiveRolesTimes)
    CurrentActiveEnableRT.extend(NextActiveEnableRT)

   
    while(True):
        AssgnGoalTime = []
        EnableRoleTime = []
        CurrentActionTemp = []
        caI = 0
        while (caI < len(CurrentAction)):
            line = CurrentAction[caI]
            tok_lst = line.split(' ')
            asgtimeindex = tok_lst.index(";")
            asgtime = tok_lst[asgtimeindex + 1]
            if (tok_lst[0] == "can_assign"):    
                AssgnGoalTime.append(str.strip(tok_lst[asgtimeindex + 3]) + '+' + str.strip(tok_lst[asgtimeindex + 1]))
            if (tok_lst[0] == "can_enable"):    
                EnableRoleTime.append(str.strip(tok_lst[asgtimeindex + 3]) + '+' + str.strip(tok_lst[asgtimeindex + 1]))
    
            caI = caI + 1

       
    caI = 0
    while (caI < len(CurrentAction)):
        line = CurrentAction[caI]
        if (line =="\n"):
            break
        tok_lst = line.split()
        asgtimeindex = tok_lst.index(";")
        asgtime = tok_lst[asgtimeindex + 1]
        targetRole = tok_lst[asgtimeindex + 3] 
        isAdded = True
        if (tok_lst[1] != 'true'):        
            strAdminTime = str.strip(tok_lst[1]) + '+' + str.strip(tok_lst[3])
        if (not(strAdminTime in AssgnGoalTime) or not(strAdminTime in EnableRoleTime)):
            isAdded = False

        j = 5 
        while (j < asgtimeindex and isAdded == True):
        
            if (tok_lst[j] == targetRole): 
                isAdded = False  
                break    
            if (tok_lst[0] == "can_assign" and tok_lst[j][0] != '-' and tok_lst[j] != 'true'  and not((str.strip(tok_lst[j]) + "+" + asgtime) in AssgnGoalTime)) :
                isAdded = False
                break
            if (tok_lst[0] == "can_enable" and tok_lst[j][0] != '-' and tok_lst[j] != 'true'  and not((str.strip(tok_lst[j]) + "+" + asgtime) in EnableRoleTime)) :
                isAdded = False
                break
            j = j + 1

      
        if (isAdded) :
            CurrentActionTemp.append(" ".join(tok_lst) + "\n")
            
        caI = caI + 1

    if (len(CurrentActionTemp) == len(CurrentAction)):
        break
    else:
        CurrentAction = CurrentActionTemp
        
    print CurrentAction
    o_file.write("".join(CurrentAction))
    i_file.close()
    o_file.close()
    
    
    #######


    os.system('./agtrbac2mcmt1NSA.py system agtrbac ' + sys.argv[3] + " " + sys.argv[4] + " " + sys.argv[4] + " " + qr + " " + sys.argv[5] + " " + sys.argv[6])

os.system('echo "Running MCMT now..."' )
os.system('echo "==========================================="' )
p = os.popen('./../mcmt -v0 agtrbac')
for line in p.readlines():
    print line

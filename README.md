# Mohawk-T Testing Tool
Authors: Jonathan M. Shahen <jmshahen@uwaterloo.ca>

## Software Used
Mohawk: 
ASAPTime: Ranise
TRole & TRule: Uzun

# Description
This program takes in a folder and searches for the following files that have a specific, 
but can be customized, for the following programs: 

* Mohawk ARBAC
* ASAPTime Separate Administrator
* ASAPTime Non-Separate Administrator (specify which role is the administrator for each rule)
* TRole
* TRule 

# Installation


# Testing
Source: <http://nusmv.fbk.eu/faq.html#003>

Q: I was doing some checks when NuSMV went to segmentation fault. I
believe my model is correctly written, have you any idea how to fix
the problem?
A:
One reason could be a low stack size limit: NuSMV still uses some
recursive functions.
Under Linux/Unix generally it does not create problems to remove this
limitation entirely; with (ba)sh issue "ulimit -s unlimited", with
(t)csh "limit stacksize unlimited".
Under Windows the stack size is hard-coded in the executable. It is
possible to use the visual studio utility editbin.exe to increase the
stack size. However, since this memory will be reserved, it is better
to set it to a reasonable amount (e.g. 10 MB). The command is "editbin
/stack:10485760 NuSMV.exe". To check that it worked "dumpbin /headers
NuSMV.exe" and look for string "size of stack reserve".
package mohawk.T.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunMohawkTest extends RunTest {

    public String mode = "bmc";

    public RunMohawkTest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    public RunMohawkTest(File specFile, String procPath, String mode) {
        super(specFile, procPath);
        this.mode = mode;
    }

    @Override
    public List<String> getCommandLineString() {
        return Arrays.asList("java", "-Xmx4g", "-jar", _procFile.getAbsolutePath(), "-mode", mode, "-run", "all",
                "-input", _specFile.getAbsolutePath());
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getMohawkSearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getMohawkSearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}

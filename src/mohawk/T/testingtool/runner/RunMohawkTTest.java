package mohawk.T.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunMohawkTTest extends RunTest {
    public boolean abstractionRefinement = false;
    public String mode = "smc";

    public RunMohawkTTest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    public RunMohawkTTest(File specFile, String procPath, String mode, boolean abstractionRefinement) {
        super(specFile, procPath);
        this.mode = mode;
        this.abstractionRefinement = abstractionRefinement;
    }

    @Override
    public List<String> getCommandLineString() {
        return Arrays.asList("java", "-Xmx4g", // Java specific parameters to increase RAM
                "-jar", _procFile.getAbsolutePath(), // Location of the Mohawk+T JAR file
                "-mode", mode, // SMC or BMC mode
                "-run", "all", // This means to run all of the tool-chain and get a result
                "-loglevel", "quiet", // Turn off all logging except for WARNING AND SEVERE messages (the result is a
                                      // severe message)
                ((abstractionRefinement) ? "-absref" : ""), // Whether to use abstraction refinement or not
                "-input", _specFile.getAbsolutePath() // The input Mohawk+T file
        );
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getMohawkTSearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getMohawkTSearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}

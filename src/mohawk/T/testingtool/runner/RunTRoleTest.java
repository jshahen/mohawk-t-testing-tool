package mohawk.T.testingtool.runner;

import java.io.File;

public class RunTRoleTest extends RunTRuleTest {

    public String mode = "TRole";

    public RunTRoleTest(String procPath, int rules, int roles, int timeslots) {
        super(procPath, rules, roles, timeslots);
    }

    public RunTRoleTest(File specFile, String procPath) {
        super(specFile, procPath);
    }
}

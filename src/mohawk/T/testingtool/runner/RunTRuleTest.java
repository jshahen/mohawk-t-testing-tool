package mohawk.T.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunTRuleTest extends RunTest {

    public String mode = "TRule";

    public int _rules;
    public int _roles;
    public int _timeslots;

    public RunTRuleTest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    public RunTRuleTest(String procPath, int rules, int roles, int timeslots) {
        super(null, procPath);

        _rules = rules;
        _roles = roles;
        _timeslots = timeslots;
    }

    @Override
    public List<String> getCommandLineString() {
        if (_specFile == null) {
            // RANDOM TEST
            // ROLES RULES TIMESLOTS USER
            return Arrays.asList(_procFile.getAbsolutePath(), "" + _roles, "" + _rules, "" + _timeslots,
                    "" + (_roles - 1));
        } else {
            // SPECIFIC TEST CASE
            return Arrays.asList(_procFile.getAbsolutePath(), _specFile.getAbsolutePath());
        }
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getTRuleSearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getTRuleSearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}

package mohawk.T.testingtool.runner;

import java.io.*;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import org.apache.commons.lang3.NotImplementedException;

import mohawk.global.results.ExecutionResult;

public class RunTest implements Callable<ExecutionResult> {
    public static final Logger logger = Logger.getLogger("mohawk");
    public ProcessBuilder builder = null;
    public Process execProcess = null;
    public StringBuilder _lastError = null;
    public StringBuilder _lastOutput = null;
    public File _specFile;
    /** path that points to the runnable object (ex: JAR, EXE) */
    public File _procFile;
    /** Name to attach to the results */
    public String resultsName = "N/A";

    /** If the solver has multiple modes, then use this field to specify which is being used */
    public String mode = "";
    /** Stores the last bound used by the model checker, null if the mode doesn't support a bound */
    public Double lastBound = null;

    // Check Strings
    public String _checkReachable = null;
    public String _checkUnreachable = null;

    public RunTest(File specFile, String procPath) {
        _specFile = specFile;
        _procFile = new File(procPath);
    }

    @Override
    public ExecutionResult call() throws Exception {
        String strLine = null;
        // Grab the specific commandline for this test
        List<String> commands = getCommandLineString();
        logger.info("[COMMANDS] " + commands);

        // Execute the command
        // Runtime rt = Runtime.getRuntime();
        // Process execProcess = rt.exec(commands);
        builder = new ProcessBuilder(commands);
        builder.directory(_procFile.getParentFile());
        builder.redirectErrorStream(true);// combine the Error into the normal stream
        execProcess = builder.start();

        // Setup buffered readers for both the default output stream of the process and it's error stream
        BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));

        // Read the program's output
        _lastOutput = new StringBuilder();
        System.out.print("Reading Lines");
        int lineCount = 0;
        while ((strLine = bufread.readLine()) != null) {
            lineCount++;
            _lastOutput.append("\t" + strLine + "\n");
            System.out.print('.');
            if (lineCount % 50 == 0) {
                System.out.println("");
            }
        }
        System.out.println("Done Running");
        logger.info("Exit Value: " + execProcess.exitValue());
        logger.info("[OUTPUT BUFFER]  -----------------------------------------\n" + _lastOutput.toString()
                + "\n----------------------------------------- [OUTPUT BUFFER]");

        execProcess.waitFor();
        // check the program's output and error strings to determine the result
        setupCheckStr();
        ExecutionResult execResult = checkResult(_lastOutput.toString());

        return execResult;
    }

    @Override
    public void finalize() throws Throwable {
        if (execProcess != null) {
            execProcess.destroy();
        }
        super.finalize();
    }

    /** @param procOutput
     * @return */
    public ExecutionResult checkResult(String procOutput) {
        ExecutionResult execResult = ExecutionResult.ERROR_OCCURRED;

        Boolean reachable = procOutput.contains(_checkReachable);
        Boolean unreachable = procOutput.contains(_checkUnreachable);

        if (reachable && unreachable) {
            logger.warning("Found both REACHABLE and UNREACHABLE Strings!!");
            execResult = ExecutionResult.ERROR_OCCURRED;
        } else if (reachable) {
            logger.fine("[RESULT] Goal is Reachable");
            execResult = ExecutionResult.GOAL_REACHABLE;
        } else if (unreachable) {
            logger.fine("[RESULT] Goal is Unreachable");
            execResult = ExecutionResult.GOAL_UNREACHABLE;
        } else {
            logger.warning("[RESULT] Unable to find a result!!");
        }

        return execResult;
    }

    public List<String> getCommandLineString() {
        throw new NotImplementedException("getCommandLineString must be implemented by it's subclass");
    }

    public void setupCheckStr() {
        throw new NotImplementedException("setupCheckStr must be implemented by it's subclass");
    }

}

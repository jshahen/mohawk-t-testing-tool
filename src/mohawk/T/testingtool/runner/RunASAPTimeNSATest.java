package mohawk.T.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunASAPTimeNSATest extends RunTest {

    public String mode = "NSA";

    public RunASAPTimeNSATest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    @Override
    public List<String> getCommandLineString() {
        return Arrays.asList("python", _procFile.getAbsolutePath(), "-rNSA", _specFile.getAbsolutePath());
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getASAPTimeNSASearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getASAPTimeNSASearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}

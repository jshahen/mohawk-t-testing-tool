package mohawk.T.testingtool.runner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import mohawk.global.results.ExecutionResult;

public class RunASAPTimeSATest extends RunTest {

    public String mode = "SA";

    public RunASAPTimeSATest(File specFile, String procPath) {
        super(specFile, procPath);
    }

    @Override
    public List<String> getCommandLineString() {
        return Arrays.asList("python", "./" + _procFile.getName(), "-rSA", _specFile.getAbsolutePath());
    }

    @Override
    public void setupCheckStr() {
        _checkReachable = ExecutionResult.GOAL_REACHABLE.getASAPTimeSASearchString();
        _checkUnreachable = ExecutionResult.GOAL_UNREACHABLE.getASAPTimeSASearchString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Command Line: " + getCommandLineString());
        return sb.toString();
    }
}

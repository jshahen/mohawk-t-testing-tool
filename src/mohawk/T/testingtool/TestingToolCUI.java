package mohawk.T.testingtool;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.lang3.StringUtils;

public class TestingToolCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "MohawkT_TestingToolCUI_PreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        try {
            TestingToolInstance inst = new TestingToolInstance();

            ArrayList<String> argv = new ArrayList<String>();
            String cmd = "";
            Scanner user_input = new Scanner(System.in);

            Options options = new Options();
            inst.setupOptions(options);
            HelpFormatter f = new HelpFormatter();
            f.printHelp(120, "mohawk", StringUtils.repeat("-", 80), options, StringUtils.repeat("-", 80), true);

            printCommonCommands();

            System.out.print("Enter Commandline Argument ('!e' to run): ");
            String quotedStr = "";
            StringBuilder fullCommand = new StringBuilder();
            while (true) {
                cmd = user_input.next();
                fullCommand.append(cmd + " ");

                if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                    System.out.println("Starting: " + cmd);
                    quotedStr = cmd.substring(1);
                    continue;
                }
                if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                    System.out.println("Ending: " + cmd);
                    argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                    quotedStr = "";
                    continue;
                }

                if (!quotedStr.isEmpty()) {
                    quotedStr = quotedStr + " " + cmd;
                    continue;
                }

                if (cmd.equals("!e")) {
                    break;
                }

                if (cmd.equals("!p")) {
                    argv.clear();
                    argv.addAll(Arrays.asList(previousCmd.split(" ")));
                    break;
                }

                argv.add(cmd);
            }
            user_input.close();

            System.out.println("Commands: " + argv);

            FileWriter fw;
            try {
                if (!cmd.equals("!p")) {
                    fw = new FileWriter(previousCommandFilename, false);
                    fw.write(fullCommand.toString());
                    fw.close();
                }
            } catch (IOException e) {
                System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }

            inst.run(argv.toArray(new String[1]));
        } catch (Exception e) {
            e.printStackTrace();

            System.exit(-1);
        }
    }

    public static void printCommonCommands() {
        System.out.println("\n\n--- Common Commands ---");
        // {equallyRandom, 1/2Empty, 3/5Empty}
        System.out.println("-test_all !e");

        System.out.println("\n###################################################################");
        System.out.println("RANDOM TESTS");
        System.out.println("-run random -test_trole !e");
        System.out.println("-run random -test_trule !e");
        System.out.println("-run random -test_mohawk !e");
        System.out.println("-run random -test_mohawk_t !e");
        System.out.println("-run random -test_asaptime_sa !e");
        System.out.println("-run random -test_asaptime_nsa !e");

        System.out.println("\n###################################################################");
        System.out.println("From original Mohawk paper: Positive Conditions Only");
        System.out.println("-run normal -test_trole -input data/trole-trule/mohawk/positive !e");
        System.out.println("-run normal -test_trule -input data/trole-trule/mohawk/positive !e");
        System.out.println("-run normal -test_mohawk -input data/mohawk/mohawk/positive !e");
        System.out.println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/mohawk/positive !e");
        System.out.println("-run normal -test_asaptime_sa -input data/asasptime_sa/mohawk/positive !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/asasptime_nsa/mohawk/positive !e");

        System.out.println("\n###################################################################");
        System.out.println("From original Mohawk paper: Mixed Conditions without any CanRevoke commands");
        System.out.println("-run normal -test_trole -input data/trole-trule/mohawk/mixednocr !e");
        System.out.println("-run normal -test_trule -input data/trole-trule/mohawk/mixednocr !e");
        System.out.println("-run normal -test_mohawk -input data/mohawk/mohawk/mixednocr !e");
        System.out.println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/mohawk/mixednocr !e");
        System.out.println("-run normal -test_asaptime_sa -input data/asasptime_sa/mohawk/mixednocr !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/asasptime_nsa/mohawk/mixednocr !e");

        System.out.println("\n###################################################################");
        System.out.println("From original Mohawk paper: Mixed Conditions and CanRevoke commands");
        System.out.println("-run normal -test_trole -input data/trole-trule/mohawk/mixed !e");
        System.out.println("-run normal -test_trule -input data/trole-trule/mohawk/mixed !e");
        System.out.println("-run normal -test_mohawk -input data/mohawk/mohawk/mixed !e");
        System.out.println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/mohawk/mixed !e");
        System.out.println("-run normal -test_asaptime_sa -input data/asasptime_sa/mohawk/mixed !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/asasptime_nsa/mohawk/mixed !e");

        System.out.println("\n###################################################################");
        System.out.println("Ranise Testsuite B");
        System.out.println("-run normal -test_trole -input data/TestsuiteB !e");
        System.out.println("-run normal -test_trule -input data/TestsuiteB !e");
        System.out.println("-run normal -test_mohawk -input data/TestsuiteB !e");
        System.out.println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/ranise/testsuiteb !e");
        System.out.println("-run normal -test_asaptime_sa -input data/TestsuiteB !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/TestsuiteB !e");

        System.out.println("\n###################################################################");
        System.out.println("Ranise Testsuite C (Only testing Mohawk, Mohawk+T, ASASPTime NSA and ASASPTime SA)");
        System.out.println("-run normal -test_mohawk -input data/TestsuiteC/AGTHos !e");
        System.out.println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/ranise/testsuitec/hos !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/TestsuiteC/AGTHos !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/TestsuiteCOrig/AGTHos !e");
        System.out.println("-run normal -test_mohawk -input data/TestsuiteC/AGTUniv !e");
        System.out
                .println("-run normal -test_mohawk_t -absref -repeat 5 -input data/mohawkT/ranise/testsuitec/univ !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/TestsuiteC/AGTUniv !e");
        System.out.println("-run normal -test_asaptime_nsa -input data/TestsuiteCOrig/AGTUniv !e");

        System.out.println("\n###################################################################");
        System.out.println("EXTRA Tests Designed by Mahesh Tripunitara");
        System.out.println("-run normal -test_mohawk -input /media/jmshahen/Backup/Mahesh/easy !e");
        System.out.println("-run normal -test_asaptime_nsa -input /media/jmshahen/Backup/Mahesh/easy !e");
        System.out.println("-run normal -test_asaptime_sa -input /media/jmshahen/Backup/Mahesh/easy !e");

        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command: " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}

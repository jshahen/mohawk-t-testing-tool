package mohawk.T.testingtool.tools;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import mohawk.T.testingtool.TestingToolInstance;
import mohawk.T.testingtool.helper.TestSuiteA;

public class QuickTester {
    public static String program = "./trule.exe";
    public static Integer repeat = 5;

    public static void main(String[] args) throws IOException, InterruptedException {
        TestingToolInstance tti = new TestingToolInstance();
        ArrayList<TestSuiteA> tests = tti.setupRandomTests();

        Long[][] results = new Long[tests.size()][repeat];
        String resultsString = "";
        Long start, end;

        Process execProcess;
        TestSuiteA t;
        for (int j = 0; j < tests.size(); j++) {
            t = tests.get(j);
            resultsString += t + ",";
            for (int i = 0; i < repeat; i++) {
                System.out.println("Test: " + t);
                start = System.currentTimeMillis();

                String[] commands;
                if (program.equals("./trole.exe")) {
                    // role-schedule <# of roles> <# of rules> <# of time slots> <# of users>
                    commands = new String[]{program, t.numberOfRoles.toString(), t.numberOfRules.toString(),
                            t.numberOfTimeslots.toString(), "2"};
                } else {
                    // rule-schedule <# of roles> <# of rules> <max time>
                    commands = new String[]{program, t.numberOfRoles.toString(), t.numberOfRules.toString(),
                            t.numberOfTimeslots.toString()};
                }

                ProcessBuilder builder = new ProcessBuilder(commands);
                builder.redirectErrorStream(true); // combine the Error into the normal stream
                execProcess = builder.start();

                BufferedReader bufread = new BufferedReader(new InputStreamReader(execProcess.getInputStream()));

                String strLine = null;
                StringBuilder _lastOutput = new StringBuilder();
                System.out.print("Reading Lines");
                while ((strLine = bufread.readLine()) != null) {
                    _lastOutput.append("\t" + strLine + "\n");
                    System.out.print('.');
                }
                System.out.println("Done Reading\n");

                System.out.println(_lastOutput.toString());
                execProcess.waitFor();

                end = System.currentTimeMillis();
                results[j][i] = end - start;
                resultsString += (end - start) + ", ";
            }
            resultsString += "\n";
        }

        Files.write(Paths.get(program + "_results.csv"), resultsString.getBytes());

    }
}

package mohawk.T.testingtool.helper;

public class TestSuiteA {
    public Integer numberOfRoles;
    public Integer numberOfRules;
    public Integer numberOfTimeslots;

    public TestSuiteA(Integer numberOfRoles, Integer numberOfRules, Integer numberOfTimeslots) {
        this.numberOfRoles = numberOfRoles;
        this.numberOfRules = numberOfRules;
        this.numberOfTimeslots = numberOfTimeslots;
    }

    @Override
    public String toString() {
        return "[Roles: " + numberOfRoles + "; Rules: " + numberOfRules + "; Timeslots: " + numberOfTimeslots + "]";
    }
}

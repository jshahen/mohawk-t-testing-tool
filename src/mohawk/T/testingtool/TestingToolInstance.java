package mohawk.T.testingtool;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import mohawk.T.testingtool.helper.ProcessHelper;
import mohawk.T.testingtool.helper.TestSuiteA;
import mohawk.T.testingtool.runner.*;
import mohawk.global.convert.mohawk.ConvertToMohawk;
import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.generators.*;
import mohawk.global.helper.FileExtensions;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.reduced.ASAPTimeNSA;
import mohawk.global.pieces.reduced.ASAPTimeSA;
import mohawk.global.results.MohawkResults;
import mohawk.global.results.TestingResult;
import mohawk.global.timing.MohawkTiming;

public class TestingToolInstance {
    private static final String VERSION = "v0.0.2";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen@uwaterloo.ca>";
    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private String Logger_filepath = "mohawk-testing-tool.log.csv";
    private String Logger_folderpath = "logs";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String resultsFile = "logs/latest_Mohawk-T_Testing_Tool_Results.csv";
    private String timingFile = "logs/latest_Mohawk-T_Testing_Tool_Timing_Results.csv";

    // Helpers
    public ProcessHelper fileHelper = new ProcessHelper();
    public MohawkTiming timing = new MohawkTiming();
    public MohawkResults results = new MohawkResults();
    public File inputFolder = new File(".").getAbsoluteFile();
    public FileExtensions fileExt = new FileExtensions();

    // Paths to external programs
    public String programFolder = "./programs";
    public String pathASAPTime_SA = programFolder + "/ASASPTIME/ASASystem.py";
    public String pathASAPTime_NSA = programFolder + "/ASASPTIME/ASASystem.py";
    public String pathMohawk = programFolder + "/mohawk/mohawk-2.0.jar";
    public String pathMohawkT = programFolder + "/mohawkT/mohawkT.jar";
    public String pathTRole = programFolder + "/trole/trole.exe";
    public String pathTRule = programFolder + "/trule/trule.exe";

    // SETTINGS
    public int repeat = 5;
    public boolean abstractionRefinement = false;

    private Path tempPath = Paths.get("temporarySpecFile.txt");

    /** 
     * @param args
     * @return 0 if it completed without error otherwise it is an error code 
     */
    public int run(String[] args) {
        try {
            ////////////////////////////////////////////////////////////////////////////
            // COMMAND LINE OPTIONS
            Options options = new Options();
            setupOptions(options);

            CommandLineParser cmdParser = new BasicParser();
            CommandLine cmd = cmdParser.parse(options, args);

            setupLoggerOptions(cmd, options);

            if (setupReturnImmediatelyOptions(cmd, options)) { return 0; }

            /* Timing */timing.startTimer("totalTime");

            setupUserPreferenceOptions(cmd, options);

            setupSpecOptions(cmd, options);

            setupResultOptions(cmd, options);
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // TIMING
            // Check if the timing file exists to know if the header should be written
            File _timingFile = new File(timingFile);
            Boolean writeHeader = false;
            if (!_timingFile.exists()) {
                writeHeader = true;
            }

            FileWriter timingWriter = new FileWriter(_timingFile, true);
            if (writeHeader) {
                logger.info("[TIMING] Creating the timing file and writing the CSV header to the file: "
                        + _timingFile.getAbsolutePath());
                timing.writeHeader(timingWriter);
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // RESULTS
            // Check if the timing file exists to know if the header should be written
            File _resultsFile = new File(resultsFile);
            Boolean writeResultsHeader = false;
            if (!_resultsFile.exists()) {
                writeResultsHeader = true;
            }

            FileWriter resultsWriter = new FileWriter(_resultsFile, true);
            if (writeResultsHeader) {
                logger.info("[RESULTS] Creating the results file and writing the CSV header to the file: "
                        + _resultsFile.getAbsolutePath());
                results.writeHeader(resultsWriter);
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // RUN ACTION
            if (cmd.hasOption("run")) {
                if (cmd.getOptionValue("run").equals("normal")) {
                    runNormalTests(cmd, resultsWriter, timingWriter);
                } else if (cmd.getOptionValue("run").equals("random")) {
                    runRandomTests(cmd, resultsWriter, timingWriter);
                } else {
                    logger.severe("UNKNOWN ACTION: " + cmd.getOptionValue("run"));
                }
            } else {
                logger.severe("NO RUN ACTION PROVIDED");
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            /* Timing */timing.stopTimer("totalTime");
            /* Timing */timing.writeOutLast(timingWriter);
            /* Timing */timingWriter.close();

            resultsWriter.close();
            ////////////////////////////////////////////////////////////////////////////

            logger.info("[TIMING] " + timing.toString());
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            logger.info("[EOF] The Testing Tool Instance is now done");
            return -1;
        }

        logger.info("[EOF] The Testing Tool Instance is now done");
        return 0;
    }

    /** Simple function to return the logger level
     * 
     * @return the logging level set at program runtime by the commandline 
     */
    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    /** Checks for each of the programs using the paths after they have been updated by the commandline parameters
     * 
     * @return true for if all programs exists and false if at least one doesn't exists 
     */
    private Boolean checkPrograms() {
        Boolean result = true;
        File tmp;

        tmp = new File(pathASAPTime_NSA);
        System.out.print("Checking for " + pathASAPTime_NSA + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        tmp = new File(pathASAPTime_SA);
        System.out.print("Checking for " + pathASAPTime_SA + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        tmp = new File(pathMohawk);
        System.out.print("Checking for " + pathMohawk + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        tmp = new File(pathMohawkT);
        System.out.print("Checking for " + pathMohawkT + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        tmp = new File(pathTRole);
        System.out.print("Checking for " + pathTRole + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        tmp = new File(pathTRule);
        System.out.print("Checking for " + pathTRule + " ... ");
        if (!tmp.isFile()) {
            System.out.println("Unable to locate!");
            result = false;
        } else {
            System.out.println("Found");
        }

        return result;
    }

    /** Checks the commandline options to see if the spec file extension (ext) is
     * currently active
     * 
     * @param cmd holds the current commandline parameters the user has entered
     * @param ext holds the file extension to see if it has been activated
     * @return 
     */
    private boolean testExtention(CommandLine cmd, String ext) {
        // Program all exceptions above here
        if (cmd.hasOption("test_all")) { return true; }

        if (ext.equals(fileExt.ASASPTime_NSA) && cmd.hasOption("test_asaptime_nsa")) { return true; }

        if (ext.equals(fileExt.ASASPTime_SA) && cmd.hasOption("test_asaptime_sa")) { return true; }

        if (ext.equals(fileExt.TRole) && cmd.hasOption("test_trole")) { return true; }

        if (ext.equals(fileExt.TRule) && cmd.hasOption("test_trule")) { return true; }

        if (ext.equals(fileExt.Mohawk) && cmd.hasOption("test_mohawk")) { return true; }

        if (ext.equals(fileExt.Mohawk_T) && cmd.hasOption("test_mohawk_t")) { return true; }

        return false;
    }

    public ArrayList<TestSuiteA> setupRandomTests() {
        ArrayList<TestSuiteA> tests = new ArrayList<TestSuiteA>();
        Integer defaultVal = 200;
        Integer defaultValTime = 20;
        Integer[] testValues = new Integer[]{100, 300, 500, 700, 1000};

        // For Roles and Rules
        for (int i = 0; i < 2; i++) {
            for (Integer j : testValues) {
                if (i == 0) {
                    tests.add(new TestSuiteA(j, defaultVal, defaultValTime));
                } else {
                    tests.add(new TestSuiteA(defaultVal, j, defaultValTime));
                }
            }
        }

        // For Time Slots
        Integer[] testValuesTS = new Integer[]{10, 20, 40, 60, 80, 100};
        for (Integer j : testValuesTS) {
            tests.add(new TestSuiteA(defaultVal, defaultVal, j));
        }

        return tests;
    }

    private void runRandomTests(CommandLine cmd, FileWriter resultsWriter, FileWriter timingWriter) throws IOException {
        ArrayList<TestSuiteA> tests = setupRandomTests();

        String generatedStr = "";
        String[] fileExts = fileExt.getFileExtensions();
        for (Integer j = 0; j < fileExts.length; j++) {
            String ext = fileExts[j];
            String s = ext.substring(1); // Remove the leading period

            if (!testExtention(cmd, ext)) {
                logger.info("[FILE EXT] Skipping: " + s);
                continue;
            }
            logger.info("[FILE EXT] Executing Random Testcases for: " + s);

            FileWriter csvResults = new FileWriter(Logger_folderpath + "/" + "compactResults_random" + ext + ".csv",
                    true);
            csvResults.write("Test Name,Roles,Rules,Timeslots,"
                    + "Timing 1(ms),Result 1,Timing 2(ms),Result 2,Timing 3(ms),Result 3,"
                    + "Timing 4(ms),Result 4,Timing 5(ms),Result 5\n");
            for (TestSuiteA test : tests) {
                csvResults.write(s + "," + test.numberOfRoles + "," + test.numberOfRules + ","//
                        + test.numberOfTimeslots);
                for (int i = 0; i < repeat; i++) {
                    String timerStr = s + " - " + test + " (" + i + ")";
                    /* Timing */timing.startTimer(timerStr);

                    logger.info("[Running] " + timerStr);

                    //////////////////////////////////////////////////////////////////////////////
                    // Generate the new Random Testcase
                    /* Timing */timing.startTimer(timerStr + "_generating");
                    if (ext.equals(fileExt.Mohawk) || ext.equals(fileExt.Mohawk_T)) {
                        MohawkTRandomGenerator genSA = new MohawkTRandomGenerator();
                        MohawkT testT = genSA.generate(test.numberOfRoles, test.numberOfTimeslots, test.numberOfRules);

                        if (ext.equals(fileExt.Mohawk_T)) {
                            generatedStr = testT.getString("\n\n", true, true);
                        } else {
                            ConvertToMohawk convert = new ConvertToMohawk(timing);
                            generatedStr = convert.convert(testT, null, false);
                        }
                    } else if (ext.equals(fileExt.ASASPTime_NSA)) {
                        // Generate a random ASAPTime NSA instance
                        ASAPTimeNSARandomGenerator genNSA = new ASAPTimeNSARandomGenerator();
                        ASAPTimeNSA testNSA = genNSA.generate(test.numberOfRoles, test.numberOfTimeslots,
                                test.numberOfRules);

                        generatedStr = testNSA.getString();
                    } else if (ext.equals(fileExt.ASASPTime_SA)) {
                        // Generate a random ASAPTime SA instance
                        ASAPTimeSARandomGenerator genSA = new ASAPTimeSARandomGenerator();
                        ASAPTimeSA testSA = genSA.generate(test.numberOfRoles, test.numberOfTimeslots,
                                test.numberOfRules);

                        generatedStr = testSA.getString();
                    } else if (ext.equals(fileExt.TRole) || ext.equals(fileExt.TRule)) {
                        // It is already built into the program (just run it from the commandline)
                    } else {
                        logger.severe("UNKNOWN EXTENSION: " + ext);
                        break;
                    }
                    /* Timing */timing.stopTimer(timerStr + "_generating");
                    /* Timing */timing.writeOutLast(timingWriter);
                    // Generate the new Random Testcase
                    //////////////////////////////////////////////////////////////////////////////

                    //////////////////////////////////////////////////////////////////////////////
                    // Save to random testcase to file (except for TROLE and TRULE)
                    if (ext.equals(fileExt.TRole) || ext.equals(fileExt.TRule)) {
                        // DO NOTHING
                    } else {
                        logger.info("Writing file out to temporary location");
                        Files.write(tempPath, generatedStr.getBytes());
                    }
                    // Save to random testcase to file (except for TROLE and TRULE)
                    //////////////////////////////////////////////////////////////////////////////

                    //////////////////////////////////////////////////////////////////////////////
                    // Run the test on the saved random testcase or with the internal program
                    logger.info("Running the random testcase ...");
                    /* Timing */timing.startTimer(timerStr + "_running");
                    RunTest runTest = null;

                    if (ext.equals(fileExt.Mohawk)) {
                        if (cmd.hasOption("smc")) {
                            runTest = new RunMohawkTest(tempPath.toFile(), pathMohawk, "smc");
                        } else {
                            runTest = new RunMohawkTest(tempPath.toFile(), pathMohawk, "bmc");
                        }
                    } else if (ext.equals(fileExt.Mohawk_T)) {
                        if (cmd.hasOption("smc")) {
                            runTest = new RunMohawkTTest(tempPath.toFile(), pathMohawkT, "smc", abstractionRefinement);
                        } else {
                            runTest = new RunMohawkTTest(tempPath.toFile(), pathMohawkT, "bmc", abstractionRefinement);
                        }
                    } else if (ext.equals(fileExt.ASASPTime_NSA)) {
                        runTest = new RunASAPTimeNSATest(tempPath.toFile(), pathASAPTime_NSA);
                    } else if (ext.equals(fileExt.ASASPTime_SA)) {
                        runTest = new RunASAPTimeSATest(tempPath.toFile(), pathASAPTime_SA);
                    } else if (ext.equals(fileExt.TRole)) {
                        runTest = new RunTRoleTest(pathTRole, test.numberOfRules, test.numberOfRoles,
                                test.numberOfTimeslots);
                    } else if (ext.equals(fileExt.TRule)) {
                        runTest = new RunTRuleTest(pathTRule, test.numberOfRules, test.numberOfRoles,
                                test.numberOfTimeslots);
                    } else {
                        logger.severe("UNKNOWN EXTENSION: " + ext);
                        break;
                    }
                    logger.info("... Done Running the random testcase");

                    TestingResult testResult = fileHelper.runProcess(runTest);
                    logger.info("[RESULT] " + testResult);
                    // Run the test on the saved random testcase or with the internal program
                    //////////////////////////////////////////////////////////////////////////////

                    //////////////////////////////////////////////////////////////////////////////
                    // Write the results to the file
                    results.add(testResult);
                    results.writeOutLast(resultsWriter);

                    /* Timing */timing.stopTimer(timerStr + "_running");
                    /* Timing */timing.writeOutLast(timingWriter);
                    csvResults.write("," + timing.getLastElapsedTime() + "," + testResult._result);

                    /* Timing */timing.stopTimer(timerStr);
                    /* Timing */timing.writeOutLast(timingWriter);
                    // Write the results to the file
                    //////////////////////////////////////////////////////////////////////////////
                }
                csvResults.write("\n");
            }
            csvResults.close();
        }
    }

    private void runNormalTests(CommandLine cmd, FileWriter resultsWriter, FileWriter timingWriter) throws IOException {
        String[] fileExts = fileExt.getFileExtensions();
        for (Integer j = 0; j < fileExts.length; j++) {
            String ext = fileExts[j];

            if (!testExtention(cmd, ext)) {
                logger.info("[FILE EXT] Skipping file extension: " + ext);
                continue;
            }
            logger.info("[FILE EXT] Processing file extension: " + ext);

            FileWriter csvResults = new FileWriter(Logger_folderpath + "/" + "compactResults" + ext + ".csv", true);
            csvResults.write("Test Name,Timing 1(ms),Result 1,Timing 2(ms),Result 2,Timing 3(ms),Result 3,"
                    + "Timing 4(ms),Result 4,Timing 5(ms),Result 5\n");
            csvResults.flush();

            File[] files = inputFolder.listFiles(FileExtensions.getFilter(ext));
            // Requires this for Linux, as it comes in randomly
            ArrayList<File> filesSorted = new ArrayList<File>(Arrays.asList(files));
            Collections.sort(filesSorted, new Comparator<File>() {
                public int compare(File p1, File p2) {
                    return p1.getName().compareToIgnoreCase(p2.getName());
                }
            });
            for (File specFile : filesSorted) {
                String timerStr = ext + " - specFile " + specFile.getAbsolutePath();
                if (cmd.hasOption("smc")) {
                    timerStr += "(SMC)";
                }
                RunTest test;

                csvResults.write(specFile.getAbsolutePath());
                csvResults.flush();

                for (Integer r = 0; r < repeat; r++) {
                    logger.info("Processing file: " + specFile.getAbsolutePath());

                    /* Timing */timing.startTimer(timerStr);

                    // Can't use a switch statement because the file extensions
                    // must be dynamic
                    if (ext.equals(fileExt.Mohawk)) {
                        logger.fine("Creating a RunMohawkTest object");
                        test = new RunMohawkTest(specFile, pathMohawk);
                        if (cmd.hasOption("smc")) {
                            ((RunMohawkTest) test).mode = "smc";
                        } else {
                            ((RunMohawkTest) test).mode = "bmc";
                        }

                    } else if (ext.equals(fileExt.Mohawk_T)) {
                        logger.fine("Creating a RunMohawkTTest object");
                        String mode = "bmc";
                        if (cmd.hasOption("smc")) {
                            mode = "smc";
                        } else {
                            mode = "bmc";
                        }
                        test = new RunMohawkTTest(specFile, pathMohawkT, mode, abstractionRefinement);

                    } else if (ext.equals(fileExt.ASASPTime_SA)) {
                        logger.fine("Creating a RunASAPTimeSATest object");
                        test = new RunASAPTimeSATest(specFile, pathASAPTime_SA);

                    } else if (ext.equals(fileExt.ASASPTime_NSA)) {
                        logger.fine("Creating a RunASAPTimeNSATest object");
                        test = new RunASAPTimeNSATest(specFile, pathASAPTime_NSA);

                    } else if (ext.equals(fileExt.TRole)) {
                        logger.fine("Creating a RunTRoleTest object");
                        test = new RunTRoleTest(specFile, pathTRole);

                    } else if (ext.equals(fileExt.TRule)) {
                        logger.fine("Creating a RunTRuleTest object");
                        test = new RunTRuleTest(specFile, pathTRule);

                    } else {
                        // Can only enter this section if someone forgot to program something in
                        logger.warning("[SKIPPING] The file extension: '" + ext
                                + "' does not have a program capable of testing it!");

                        timing.removeTimer(timerStr);
                        continue;
                    }

                    TestingResult testResult = fileHelper.runProcess(test);

                    logger.info("[RESULT] " + testResult);

                    // Write the results to the file
                    results.add(testResult);
                    results.writeOutLast(resultsWriter);

                    /* Timing */timing.stopTimer(timerStr);
                    /* Timing */timing.writeOutLast(timingWriter);

                    csvResults.write("," + timing.getLastElapsedTime() + "," + testResult._result);
                    csvResults.flush();
                }
                csvResults.write("\n");
                csvResults.flush();
            }
            csvResults.close();
        }
    }

    public ArrayList<TestSuiteA> setupRandomTests_old() {
        ArrayList<TestSuiteA> tests = new ArrayList<TestSuiteA>();
        Integer defaultVal = 200;
        Integer[] testValues = new Integer[]{100, 300, 500, 700, 1000};

        // For Roles and Rules
        for (int i = 0; i < 2; i++) {
            for (Integer j : testValues) {
                if (i == 0) {
                    tests.add(new TestSuiteA(j, defaultVal, defaultVal));
                } else {
                    tests.add(new TestSuiteA(defaultVal, j, defaultVal));
                }
            }
        }

        // For Time Slots
        Integer[] testValuesTS = new Integer[]{100, 200, 400, 600, 800, 1000};
        for (Integer j : testValuesTS) {
            tests.add(new TestSuiteA(defaultVal, defaultVal, j));
        }

        return tests;
    }

    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        options.addOption("smc", false, "Mohawk in SMC mode (Default: BMC mode)");

        // Add Information Options
        options.addOption("help", false, "Print this message");
        options.addOption("authors", false, "Prints the authors");
        options.addOption("version", false, "Prints the version (" + VERSION + ") information");
        options.addOption("checkprograms", false,
                "Checks that all the Programs are found and when run don't return a non-zero value");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create("loglevel"));
        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create("output"));
        options.addOption("noheader", false, "Does not write the CSV file header to the output log");

        options.addOption(OptionBuilder.withArgName("csvfile")
                .withDescription("The file where the result should be stored").hasArg().create("results"));

        // custom Console Logging Options
        options.addOption(OptionBuilder.withArgName("num")
                .withDescription("The maximum width of the console (default 120)").hasArg().create("maxw"));
        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create("linestr"));

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("folder path")
                .withDescription("Path to a folder that contains TARBAC Spec files; default current directory").hasArg()
                .create("input"));

        // Run action
        options.addOption(OptionBuilder.withArgName("normal|random")
                .withDescription("Run random tests ('random') or specific testcases ('normal')").hasArg()
                .create("run"));

        // Testing Options
        options.addOption(OptionBuilder.withArgName("num")
                .withDescription("Sets how many times to repeat a test case. Default = " + repeat).hasArg()
                .create("repeat"));

        options.addOption("absref", false, "Turns on Abstraction Refinement for Mohawk+T program");

        options.addOption("test_asaptime_sa", false,
                "Test all files that have the extension for ASAPTime Separate Administrator (default: "
                        + fileExt.ASASPTime_SA + ")");
        options.addOption("test_asaptime_nsa", false,
                "Test all files that have the extension for ASAPTime Non-Separate Administrator (default: "
                        + fileExt.ASASPTime_NSA + ")");
        options.addOption("test_trole", false,
                "Test all files that have the extension for TRole (default: " + fileExt.TRole + ")");
        options.addOption("test_trule", false,
                "Test all files that have the extension for TRule (default: " + fileExt.TRule + ")");
        options.addOption("test_mohawk", false,
                "Test all files that have the extension for Mohawk (default: " + fileExt.Mohawk + ")");
        options.addOption("test_mohawk_t", false,
                "Test all files that have the extension for Mohawk+T (default: " + fileExt.Mohawk_T + ")");
        options.addOption("test_all", false, "Test all file extensions");

        // program paths
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription(
                        "Exectution path for ASAPTime Separate Administrator (default: " + pathASAPTime_SA + ")")
                .hasArg().create("path_asaptime_sa"));
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription(
                        "Exectution path for ASAPTime Non-Separate Administrator (default: " + pathASAPTime_NSA + ")")
                .hasArg().create("path_asaptime_nsa"));
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription("Exectution path for TRole (default: " + pathTRole + ")").hasArg()
                .create("path_trole"));
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription("Exectution path for TRule (default: " + pathTRule + ")").hasArg()
                .create("path_trule"));
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription("Exectution path for mohawk (default: " + pathMohawk + ")").hasArg()
                .create("path_mohawk"));
        options.addOption(OptionBuilder.withArgName("filepath")
                .withDescription("Exectution path for Mohawk+T (default: " + pathMohawkT + ")").hasArg()
                .create("path_mohawk_t"));

    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.INFO); // Default Level
        if (cmd.hasOption("loglevel")) {
            String loglevel = cmd.getOptionValue("loglevel");
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.SEVERE);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption("noheader")) {
            WriteCSVFileHeader = false;
        }

        // Set Logger Folder
        if (cmd.hasOption("logfolder")) {
            File logfile = new File(cmd.getOptionValue("logfolder"));

            if (!logfile.exists()) {
                logfile.mkdir();
            }

            if (logfile.isDirectory()) {
                Logger_folderpath = cmd.getOptionValue("logfolder");
            } else {
                logger.severe("logfolder did not contain a folder that exists or that could be created!");
            }
        }

        // Set File Logger
        if (cmd.hasOption("logfile")) {
            // Check if no log file was requested
            if (cmd.getOptionValue("logfile").equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue("logfile").equals("u")) {
                // Create a unique log file
                Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(Logger_folderpath + File.separator + cmd.getOptionValue("logfile"));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true); // Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }
        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {
            File f = new File(Logger_folderpath + File.separator);
            f.mkdirs();
            fileHandler = new FileHandler(Logger_folderpath + File.separator + Logger_filepath, true);// Always append!
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    private void setupProgramOptions(CommandLine cmd, Options options) {
        // Set the Path for the ASAPTime NSA Program
        if (cmd.hasOption("path_asaptime_nsa")) {
            logger.info("[OPTION] Setting the path for ASAPTime NSA");

            pathASAPTime_NSA = cmd.getOptionValue("path_asaptime_nsa");
        } else {
            logger.info("[OPTION] Using the Default path for ASAPTime NSA");
        }

        // Set the Path for the ASAPTime SA Program
        if (cmd.hasOption("path_asaptime_sa")) {
            logger.info("[OPTION] Setting the path for ASAPTime SA");

            pathASAPTime_SA = cmd.getOptionValue("path_asaptime_sa");
        } else {
            logger.info("[OPTION] Using the Default path for ASAPTime SA");
        }

        // Set the Path for the TRole Program
        if (cmd.hasOption("path_trole")) {
            logger.info("[OPTION] Setting the path for TRole");

            pathTRole = cmd.getOptionValue("path_trole");
        } else {
            logger.info("[OPTION] Using the Default path for TRole");
        }

        // Set the Path for the TRule Program
        if (cmd.hasOption("path_trule")) {
            logger.info("[OPTION] Setting the path for TRule");

            pathTRule = cmd.getOptionValue("path_trule");
        } else {
            logger.info("[OPTION] Using the Default path for TRule");
        }

        // Set the Path for the Mohawk Program
        if (cmd.hasOption("path_mohawk")) {
            logger.info("[OPTION] Setting the path for Mohawk");

            pathMohawk = cmd.getOptionValue("path_mohawk");
        } else {
            logger.info("[OPTION] Using the Default path for Mohawk");
        }

        // Set the Path for the Mohawk+T Program
        if (cmd.hasOption("path_mohawk_t")) {
            pathMohawkT = cmd.getOptionValue("path_mohawk_t");
            logger.info("[OPTION] Setting the path for Mohawk+T to: " + pathMohawkT);
        } else {
            logger.info("[OPTION] Using the Default path for Mohawk+T: " + pathMohawkT);
        }

        // Turn on Abstraction Refinement for the Mohawk+T Program
        if (cmd.hasOption("absref")) {
            abstractionRefinement = true;
        }
        logger.info("[OPTION] Abstraction Refinement for Mohawk+T: " + abstractionRefinement);
    }

    private void setupResultOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption("results")) {
            logger.fine("[OPTION] Changing the results file");
            resultsFile = cmd.getOptionValue("results");
        }
        logger.info("[OPTION] Results File: " + resultsFile);

        if (cmd.hasOption("repeat")) {
            try {
                logger.fine("[OPTION] Changing the repeat value");
                String b = cmd.getOptionValue("repeat");
                repeat = Integer.decode(b);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'repeat': " + repeat + ";\n" + e.getMessage());
            }
        }
        logger.info("[OPTION] Repeat Testcases: " + repeat);
    }

    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption("help") == true || cmd.getOptions().length == 0) {
            HelpFormatter f = new HelpFormatter();
            if (cmd.hasOption("maxw")) {
                try {
                    Integer maxw = Integer.decode(cmd.getOptionValue("maxw"));
                    f.printHelp(maxw, "mohawk-testing", StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n"
                            + StringUtils.repeat("-", 20), options, StringUtils.repeat("-", maxw), true);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("An error occured when trying to print out the help options!");
                }

            } else {
                f.printHelp(80, "mohawk-testing", StringUtils.repeat("-", 80), options, StringUtils.repeat("-", 80),
                        true);
            }
            return true;
        }

        if (cmd.hasOption("version")) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption("authors")) {
            // keep it as simple as possible for the version
            System.out.println(AUTHORS);
            return true;
        }

        setupProgramOptions(cmd, options);
        if (cmd.hasOption("checkprograms")) {
            if (checkPrograms()) {
                System.out.println("All programs accounted for!");
            } else {
                System.out.println("Some of the programs are missing! Please see the output above for details.");
            }
            return true;
        }

        return false;
    }

    private void setupSpecOptions(CommandLine cmd, Options options) throws FileNotFoundException {
        // Grab the SPEC file
        if (cmd.hasOption("input")) {
            File tmp = new File(cmd.getOptionValue("input"));
            if (tmp.isDirectory()) {
                logger.fine("[OPTION] Using a specific SPEC Folder: " + cmd.getOptionValue("input"));
                inputFolder = tmp;
            } else {
                logger.warning("[OPTION] The specific SPEC Folder: " + cmd.getOptionValue("input")
                        + " does not point to a directory");
                throw new FileNotFoundException();
            }
        } else {
            logger.fine("[OPTION] No specific SPEC Folder included, using the current directory: "
                    + inputFolder.getAbsolutePath());
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption("maxw")) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue("maxw");
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption("linestr")) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd.getOptionValue("linestr");
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }
}

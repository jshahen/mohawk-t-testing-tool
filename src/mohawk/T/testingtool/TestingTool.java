package mohawk.T.testingtool;

public class TestingTool {
    public static void main(String[] args) {
        try {
            TestingToolInstance tti = new TestingToolInstance();

            tti.run(args);
        } catch (Exception e) {
            e.printStackTrace();

            System.exit(-1);
        }
    }
}
